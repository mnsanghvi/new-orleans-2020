#+TITLE: The Crescent City
#+OPTIONS: toc:nil num:nil
#+REVEAL_THEME: black
#+REVEAL_TRANS: default
#+REVEAL_MARGIN: 0.1
#+COMMENT: REVEAL_PLUGINS: (markdown highlight progress keyboard classList)
#+COMMENT: M-x org-reveal-export-to-html



* New Orleans 2020

*Jan 23rd - Jan 27th*

_The Paris of the South or Bust!!__


* Departing BOS

 | Date     | Airline  | Take-off   | Landing |
 |----------+----------+------------+---------|
 | Jan 23rd | Jet Blue | 16:00 (ET) |   19:59 |
 |          | B6 301   | 15:00 (CT) |   18:59 |

* Departing MSY

 | Date     | Airline  | Take-off   | Landing |
 |----------+----------+------------+---------|
 | Jan 27th | Jet Blue | 15:05 (CT) |   18:15 |
 |          | B6 400   | 14:05 (ET) |   17:15 |

* Time Zone

New Orleans is Central Time \\
  + BOS -1hr
  + DXB -10hrs

* Transportation

+ 12:00 - Depart for Woburn
+ 12:15 - Lunch to go ??? (Panera, Moe's, Chipotle )
+ 12:30 - Uber to Terminal C
+ Uber from MSY to Hotel
  + Must book after landing, cant do it in advance

* Accommodations

The Saint Hotel \\
931 Canal Street \\
New Orleans, LA 70112 \\
+1-504-522-5400 \\

+ Confirmation number:  74869648
+ Check-in:  16:00 Jan 23rd
+ Check-out: 11:00 Jan 27th

* Accommodations - Amenities 

+ Hair dryer
+ Ironing board / iron
+ Free WiFi

* Clothing

New England Autumn weather \\
Highs in low 60s \\
Lows in mid-40s

* Activities to do
+ [[https://www.atlasobscura.com/things-to-do/new-orleans-louisiana/places][Unusual Attractions in New Orleans]]
+ Voodoo Tour (Day 1 evening)
+ Plantations (Day 2 morning / afternoon)
+ Jazz (Day 0 night time)
+ Bourbon Street (every night ? )

* Food
+ Groceries ?? 

* Day 0 (Jan 23rd) - Night
+ Dinner
  + Mona Lisa New Orleans \\
    121 Royal St \\
    +1-504-522-6746 \\
+ Bourbon Street
+ Late Night Jazz

* Day 1 (Jan 24th) - Morning
+ 10:00 or earlier - head out to Jackson Square

* Day 1 (Jan 24th) - Afternoon

* Day 1 (Jan 24th) - Evening / Night
+ 18:30 Felipe's Mexican Taqueria 
  + 301 N Peters St, NOLA, 70130
  + +1-504-267-4406
+ 20:00 Voodoo Tour starts
  + 400 Royal Street
  + Confirm: BR-769636244

* Day 2 (Jan 25th) - Morning
+ 08:30 Leave Hotel and go to pick up point
  + DoubleTree Hilton \\
    300 Canal Street \\
+ Pack lunch, might not get time at plantation restaurent
+ 09:00 Depart on plantation tour
  + BR-769619398

* Day 2 (Jan 25th) - Evening / Night
+ 16:30 (17:00) - Arrive back at Hilton
  + Request drop off at The Saint if possible

* Day 3 (Jan 26th) - Morning

* Day 3 (Jan 26th) - Afternoon

* Day 3 (Jan 26th) - Evening / Night 
* Day 4 (Jan 27th) - Morning / Afternoon
+ 11:00 - Check out
+ 11:30 - Lunch ? (to go?)
+ 13:00 - Airport
